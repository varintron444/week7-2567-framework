const express = require("express");
const app = express();
require('dotenv').config()

const PORT = process.env.PORT || 3001


app.get('/', (req, res) => {
    console.log('Express!');
    return res.status(200).json({ mesage: 'GET POST request to the  Home page!' })
});

app.post('/', (req, res) => {
    console.log('Express!');
    return res.status(201).json({ mesage: 'POST request to the  Home page!' })
});

app.put('/', (req, res) => {
    console.log('Express!');
    return res.status(200).json({ mesage: 'PUT request to the  Home page!' })
});

app.delete('/', (req, res) => {
    console.log('Express!');
    return res.status(200).json({ mesage: 'DELETE request to the  Home page!' })
});



app.listen(3000, () => {
    console.log(`Server running at http://localhost:${PORT}`);
});
